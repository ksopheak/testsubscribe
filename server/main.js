import { Meteor } from 'meteor/meteor';
//import { Mongo } from 'meteor/mongo';
import { dbNews } from '../collections/news.js';

Meteor.publish('dbNews', function(){
    return dbNews.find({});
});


Meteor.methods({
    'createUser': function(firstName, lastName){
        dbNews.insert({
            fname: firstName,
            lname: lastName
        });
    }
});