import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';
import './main.html';
import {dbNews} from '../collections/news';
Meteor.subscribe('dbNews');

Template.hello.events({
  'click #save'(e) {
    e.preventDefault();
    var firstName = $('#fname').val();
    var lastName = $('#lname').val();
    Meteor.call("createUser", firstName, lastName);
  },
});
Template.hello.helpers({
   typedoc(){
    return dbNews.find({});
   },
});
